# [Stability and performance analysis of NMPC: Detectable stage costs and general terminal costs](https://arxiv.org/abs/2110.11021) - code for the paper 

# Dependencies
- [YALMIP](https://yalmip.github.io/): for offline computations + setting up linear MPC
- [CasADi](https://web.casadi.org/) + [IPOPT](https://coin-or.github.io/Ipopt/): for nonlinear MPC problems


# LinearExample_ChainOfMassSpringDampers
1. Run `main.m` to obtain stabilizing prediction horizon N for different stage costs
2. Run `MPC_simulation.m` to simulate different linear MPC schemes 
 
# NonlinearExample_FourTank
1. Run `main.m` to obtain stabilizing prediction horizon N for different stage costs
2. Run `MPC_simulation.m` to simulate different nonlinear MPC schemes 
