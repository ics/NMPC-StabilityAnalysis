function [P,K]=RicattiDiff(A,B,Q,R,P_f,N_max)
%finite-horizon RicattiDifferenceEquation
%% Input
%A,B: system model
%Q,R: cost matrices
%P_f: terminal penalty (zeros(n) if UCON)
%% Output 
%P: P(:,:,k+1) corresponds to the cost with horizon k
%K: K(:,:,k) corresponds to the finite-horizon LQR input with horizon k
%%
P(:,:,1)=P_f;
for k=1:N_max
    %compute P backwards %https://stanford.edu/class/ee363/lectures/dlqr.pdf
    K(:,:,k)=-inv(R+B'*P(:,:,k)*B)*B'*P(:,:,k)*A;
    P(:,:,k+1)=Q+A'*P(:,:,k)*A+A'*P(:,:,k)*B*K(:,:,k);
end 
end