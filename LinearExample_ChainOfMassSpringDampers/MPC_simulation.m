%simulate MPC for different configurations and make closed-loop plots
%% Acknowledgment
%The code to generate the linear MPC problems is partially adapted from code
%for the MPC/AMPC lecture at ETHZ of Melanie Zeilinger. 
%%
clear all
close all
clc
load model
[n,m]=size(B);
for option=[0,2:5]%choose which MPC parametrizations to simulate
    N=5; %set horizon to N=5, except for option 0
    if option==0
    %Comparable to (infinite-horizon) constrained LQR with very large
    %horizon + LQR terminal cost (equivalent for all initial conditions
    %where terminal state lies in maximal positive invariant set under LQR)
    N=50;
    q=q_0;
    r=r_0;
    elseif option==1
    %tuning based on positive definite stage cost  (Theorem 7) - without terminal cost
    disp('option 1: not guaranteed to be stable')
    q=2e-1;
    r=r_0;
    elseif option==2
    %tuning based on positive definite stage cost  (Theorem 7) - with finte tail terminal cost
    q=1e-1;
    r=r_0;
    elseif option==3
    %tuning based on detectability  (Theorem 8) - without terminal cost
     q=q_0;
     r=12.9;
    elseif option==4
    %tuning based on detectability (Theorem 8) - with finte tail terminal cost
     q=q_0;
     r=1.7;
    elseif option==5
        %baseline design (unstable)
        q=q_0;
        r=r_0;    
    end
    %set stage cost
    Q=C'*C+q*eye(m);
    R=r*eye(m);
    %options:0=LQR;1-2:PDF(UCON,tial); 3-4:Detect(UCON,tail), 5:unstable
    if option==5||option ==1||option==3
        P_f=zeros(n);   %no terminal cost
    elseif option==2||option==4
        %finite-tail terminal cost (in linear case without state constraints, can be compressed in
        %one matrix P_f)
        P_f=0;
        for k=0:M_extended-1
           P_f=A'*P_f*A+Q;
        end
    elseif option==0
        %LQR terminal cost
        [~,P_f]=dlqr(A,B,Q,R);
    end
    %% Set up MPC problem using Yalmip
    %state constraints
    Ax = [];
    bx = [];
    %input constraints
    Au = [1;-1];
    bu = [1; 1];
    U_set = Polyhedron(Au, bu);
    nau=size(Au,1);
    %cost
    ell = @(x,u) x' * Q * x + u' * R * u;
    V_f =@(x) x'*P_f*x;
    %Set up MPCs
    eval(['yalmip_optimizer_option' num2str(option) '=create_mpc(N, A, B, Au, bu, ell,V_f);'])
    x0=20*repmat([1;0],n/2,1);%set initial condition
    Tsim=150;
    eval(['[x_traj_option' num2str(option) ', u_traj_option' num2str(option) '] = simulation(yalmip_optimizer_option' num2str(option) ',x0, A, B,Tsim);'])
end
%% Plot
close all
figure(1) 
semilogy(abs(x_traj_option2(1,:)).^2,'b:','linewidth',2)
hold on
semilogy(abs(x_traj_option5(1,:)).^2,'black--','linewidth',2)
semilogy(abs(x_traj_option4(1,:)).^2,'r:','linewidth',2)
legend({'$\ell$','$\sigma$','unstable'},'interpreter','latex','location','northeast')
axis([1,Tsim,1e-10,500])
xlabel('$k$','interpreter','latex')
ylabel('$z_1^2$','interpreter','latex')
set(gca, 'fontname','Arial','fontsize',16)
print(['Figure/cl_z1_' num2str(M) '_' num2str(N)],'-depsc')
%%
figure(2) 
stairs(u_traj_option5(1,:),'black','linewidth',2)
hold on
stairs(u_traj_option2(1,:),'b--','linewidth',2)
stairs(u_traj_option4(1,:),'r','linewidth',2)
plot([0,Tsim],[1,1],'black','linewidth',1)
plot([0,Tsim],-1*[1,1],'black','linewidth',1)
xlabel('$k$','interpreter','latex')
ylabel('$u$','interpreter','latex')
axis([0,Tsim,-1.05,1.05])
axis([1,70,-1.05,1.05])
set(gca, 'fontname','Arial','fontsize',16)
print(['Figure/cl_u_' num2str(M) '_' num2str(N)],'-depsc')
 
%%
function yalmip_optimizer = create_mpc(N, A, B,Au, bu,ell,V_f)
%MPC with input constraints and possible terminal penalty
    n = size(A,1);
    m = size(B,2);
    % define symbolic optimization variables
    U = sdpvar(m,N-1,'full');
    X = sdpvar(n,N,'full');
    x0 = sdpvar(n,1,'full');
    objective = 0;%initialize cost
    constraints = [X(:,1) == x0]; % initial state constraint
    for k = 1:N-1
        constraints = [constraints, X(:,k+1) == A * X(:,k) + B * U(:,k)]; % dynamics constraint
        constraints = [constraints, Au * U(:,k) <= bu]; % hard input constraint
        objective = objective + ell(X(:,k),U(:,k)); %add stage cost
    end
    objective = objective + V_f(X(:,N)); %add terminal cost
    % setup optimizer object
    ops = sdpsettings('verbose',0,'solver','quadprog');%solver settings
    yalmip_optimizer = optimizer(constraints,objective,ops,x0,{objective, U, X});%set solver
end
%%

function [x_traj, u_traj] = simulation(yalmip_optimizer, x0, A, B, Tsim)
%simulate an MPC given by "yalmip_optimizer"
    u_traj = [];
    x_traj = [x0];
    u_sol_mpc = {}; 
    for k = 1:1:Tsim
        [res,flag] = yalmip_optimizer(x_traj(:,end)); % call mpc
        assert(flag==0||flag==3);%check for errors/infeasibility
        u_sol_mpc{k} = res{2};%get optimal input
        u_traj(:,end+1) = u_sol_mpc{k}(:,1);%apply input
        x_traj(:,end+1) = A * x_traj(:,end) + B * u_traj(:,end);%simulate one step
    end
    x_traj(:,end)=[];%remove last state (to have trajectories of length Tsim)
end 