function [gamma_k,lambda_max]=CostControllability(A,B,Q,R,P_f,N_max,P_sigma,local)
%% Input
%A,B: system model
%Q,R: cost matrices
%P_f: terminal penalty
%N_max: horizon for which gamma_k should be computed (N=0: should be P_f)
%P_sigma: matrix defining state measure: sigma(x)=|x|_{P_o}^2 
%local: boolean (defines if constants gamma_k should be valid globally or
%only locally)
%% Output
%-lambda_max(k): max abs. eigenvalue closed loop with horizon k-1 and finite-horizon LQR (can be used to deduce local instability)
%-cost-controllability constants gamma_k (valid locally:LQR and globally valid: open-loop u=0): 
%-gamma_k(k+1) corresponds to horizon k
%
%%
%compute finite-horizon LQR 
[P,K]=RicattiDiff(A,B,Q,R,P_f,N_max);
%compute eigenvalues to check stability
for k=1:N_max
lambda_max(k)=max(abs(eig(A+B*K(:,:,k))));
end
%compute cost controllability constant gamma_k 
if local
    %a) LQR, only locally valid
    for k=1:N_max
       gamma_k(k)=max(eig(P(:,:,k),P_sigma));
    end
else
%b) Lyapunov, globally valid
    [P_global,K_global]=RicattiDiff(A,0*B,Q,R,P_f,N_max);
    for k=1:N_max
       gamma_k(k)=max(eig(P_global(:,:,k),P_sigma));
    end
end

end