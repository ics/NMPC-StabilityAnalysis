function alpha_N=LP_solve(N,gamma_k,epsilon_o,gamma_o_underline,gamma_o_overline,epsilon_f,c_f_underline,c_f_overline)
%Compute solution of LP numerically 
%(can be used to validate analytical
%solution derived in paper)
%%Input
%gamma_k:cost controllability: gamma_k: gamma_k(k+1) corresponds to
%horizon k
%N:horizon
%epsilon_o,gamma_o_underline,gamma_o_overline:detectability constants
%epsilon_f,c_f: constant for terminal penalty
tilde_ell=sdpvar(N,1);
tilde_sigma=sdpvar(N+1,1);
tilde_W=sdpvar(N+1,1);
tilde_V=sdpvar(1,1);
tilde_V_f=sdpvar(1,1);
y=[tilde_ell;tilde_sigma;tilde_W;tilde_V;tilde_V_f];
obj=sum(tilde_ell(1+1:N-1+1))+tilde_V_f-tilde_V;
%normalize
con=[tilde_sigma(1)==1];
%non-negative
con=[con;y>=0];
%bounds W w.r.t. sigma
con=[con;tilde_W-gamma_o_overline*tilde_sigma<=0];
con=[con;tilde_W-gamma_o_underline*tilde_sigma>=0];
%decrease W
con=[con;tilde_W(1+1:N+1)-tilde_W(0+1:N-1+1)+epsilon_o*tilde_sigma(0+1:N-1+1)-tilde_ell(0+1:N-1+1)<=0];
%bound sum sum ell
for k=0:N-1
   con=[con;sum(tilde_ell(k+1:N-1+1))+tilde_V_f-gamma_k(N-k+1)*tilde_sigma(k+1)<=0]; 
end
%candidate
for k=1:N
   con=[con;sum(tilde_ell(1+1:k-1+1))+gamma_k(N-k+1+1)*tilde_sigma(k+1)-tilde_V>=0]; 
end
%candidate terminal
 con=[con;sum(tilde_ell(1+1:N-1+1))+(1+epsilon_f)*tilde_V_f-tilde_V>=0];
 %bounds terminal
 con=[con;c_f_underline*tilde_sigma(N+1)-tilde_V_f<=0];
 con=[con;c_f_overline*tilde_sigma(N+1)-tilde_V_f>=0];
ops = sdpsettings('verbose',0); 
optimize(con,obj,ops);
alpha_N=1+value(obj)/epsilon_o;
end