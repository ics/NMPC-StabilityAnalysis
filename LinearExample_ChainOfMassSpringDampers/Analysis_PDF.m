function [N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,omega,M_extended)
%Determine underline{N} using Theorem 7 based on positive definite stage cost
%%Input
%System matrices, Cost matrices, maximal horizon to consider
%LP_validation:boolean, if LP should be solved to compare solution
%local: boolean, if should only be locally valid, or even globally
%terminal: boolean, which terminal cost should be used%0:no terminal; 1:omega weighting, 2:finite-tail
%%Ouput
%LP_equiv: boolean, true if LP solution same as analytic (false if LP not
%solved by default)
%N_gamma_k: sufficient horizon with LP analysis using gamma_k; 
%LP results either globally valid (u=0) or locally (LQR)

%% design terminal cost
if terminal==0
    %no terminal cost
    P_f=0*Q; 
    epsilon_f=inf;
    c_f_underline=0;
    c_f_overline=0;
    [gamma_k,~]=CostControllability(A,B,Q,R,P_f,N_max,Q,local);
elseif terminal==1
    %simple terminal weighting    
    P_f=Q*omega;
    [gamma_k,~]=CostControllability(A,B,Q,R,P_f,N_max,Q,local);
    epsilon_f=gamma_k(2)/omega-1;
    c_f_underline=omega;
    c_f_overline=omega;
elseif terminal==2
    %finite-tail cost (local is not a meaningful comparison, since in this case also a LQR terminal penalty could be arguably used)
    if local
    K_LQR=-dlqr(A,B,Q,R);
    A_K=A+B*K_LQR;
    Q_K=Q+K_LQR'*R*K_LQR;
    else
    A_K=A;    
    Q_K=Q;
    end
    P_f=0*Q;
    for k=0:M_extended-1
    P_f=P_f+A_K^k'*Q_K*A_K^k;
    end
    epsilon_f=max(eig(A_K'*P_f*A_K+Q_K,P_f))-1;
    c_f_underline=min(eig(P_f,Q));
    c_f_overline=max(eig(P_f,Q));
    [gamma_k,~]=CostControllability(A,B,Q,R,P_f,N_max,Q,local);
end
% 
gamma_bar=max(gamma_k);
%simple analytic formula (this bound is used, if N>N_max)
N_gamma_bar=ceil(1+(log(gamma_bar)-log(1+1/epsilon_f))/(log(gamma_bar)-log(gamma_bar-1)));
%%
%1. N_gamma_k: LP bound with gamma_k 
for N=1:N_max-1
   alpha_N_gamma_k(N)=1-(gamma_k(N+1)-1)*prod((gamma_k(2+1:N+1)-1)./gamma_k(2+1:N+1))/...
       (1+1/epsilon_f- prod((gamma_k(2+1:N+1)-1)./gamma_k(2+1:N+1)));
end
%%
%make sure that monotonically by checking oposite
N_gamma_k=find(alpha_N_gamma_k<=0,1,'last')+1;
if alpha_N_gamma_k(end)<0
  %in case alpha<0 for all N, then we did not find a stabilizing horizon
   N_gamma_k=nan; 
elseif alpha_N_gamma_k(1)>0
  %in case alpha(1)>0 then N=1 is already sufficient
   N_gamma_k=1;     
end

%% check if equivalent to LP result
if LP_validation
    if isnan(N_gamma_k)
        LP_equiv=true;%no way to check LP, since N>N_max and the result is: not stable
    else 
         alpha_N_test=LP_solve(N_gamma_k,gamma_k,1,0,0,epsilon_f,c_f_underline,c_f_overline);
        if norm(alpha_N_test-alpha_N_gamma_k(N_gamma_k))<=1e-6
        LP_equiv=true;
        else 
            error('Linear program not equivalent to analytical formulation!')%this should typically never happen; If this happens, there results should still provide correct bounds, but probably one of the constants is wrong/conservatively computed (cf. conditions in Assumption 5).
        end 
    end
else
    %set to false, since not validated numerically that true
    LP_equiv=false;
end
%to avoid nan, use (more conservative) but valid bounds from gamma_bar
N_gamma_k=max(N_gamma_k,N_gamma_bar);
end