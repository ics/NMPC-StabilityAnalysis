%% main
%this script computes a sufficiently long horizon \underline{N} for
%different stage costs \ell, using the results in Theorem 7 and Theorem 8
%in [Stability and performance analysis of NMPC: Detectable stage costs and general terminal costs]
clear all
close all
clc
t=tic; %to time offline computations
plots=true;%boolean to generate plots
%% 1. Define model- Large scale mass-spring damper system
n_i=2;%number of local states
M=6; %number of subsystems
if M<3
   error('not programmed') 
end
n_x=n_i*M;%number of overall states
m=1;k=10;d=2;%mass,spring, damping constant
h=1; %sampling time
%define cont.-time system matrices \dot{x}=A_c*x+B_c*u
A_c=zeros(n_x);
%i=1:also contected to ground
A_c(1,:)=[0,1,zeros(1,n_x-n_i)];
A_c(2,:)=1/m*[-k-k,-d-d,k,d,zeros(1,n_x-2*n_i)];
%1<i<M
for i=2:M-1
A_c((i-1)*n_i+1,:)=[zeros(1,(i-1)*n_i+1),1,zeros(1,n_x-i*n_i)];
A_c(i*n_i,:)=1/m*[zeros(1,n_i*(i-2)),k,d,-2*k,-2*d,k,d,zeros(1,n_x-n_i*(i+1))];
end
%i=M, measured output
A_c(M*n_i-1,:)=[zeros(1,M*n_i-1),1];
A_c(M*n_i,:)=1/m*[zeros(1,n_x-2*n_i),k,d,-k,-d]; 
%acutation on last mass
n_u=1;
B_c=1/m*[zeros(n_x-n_i,1);0;1;];
[A,B]=c2d(A_c,B_c,h);
%output: position last mass
C=[1,0,zeros(1,n_i*(M-1))]; 
  
%Determine if system is locally stable/unstable with unconstrained finite-horizon LQR
 N_test=10;%horizon range for which local stability is checked
[~,lambda]=CostControllability(A,B,C'*C+1e-4*eye(n_x),1e-5,zeros(n_x),N_test,C'*C,0);
N_lin_unstable=[1:N_test;[lambda>1]] %determines for which horizon N, system is locally unstable 
%(should be N={2,4,5}).
 
%% Cost function
q_0=1e-5;r_0=1e-6;%initial state and input weighting
%determine range of q,r,
q_series=logspace(log(q_0)/log(10),log(q_0)/log(10)+6,10);
r_series=logspace(log(r_0)/log(10),2,10);
%grid points for epsilon_o, should be between (0,1)
epsilon_o_list=logspace(-3,log(1-1e-4)/log(10),4);

N_max=200;%maximal horizon for which the stability with gamma_k is check; for larger horizon simplified formula is used, since N>N_max is not implementable anyway
LP_validation=false;%boolean, if the LP should be solved to show/check that equivalent to analytical solution
%does not influence the correctness of the results, but significantly
%increases computation times

M_extended=10;%extended prediction horizon for finite-tail cost
omega=100;%weighting on last state
local=false;%boolean: if true, then less conservative bounds are computed using the finite-horizon LQR, which only ensure local stability;
            %         if false, then globally valid bounds are computed
            
save('model','A','B','C','q_0','r_0','omega','M_extended','M');%save model for simulation
%% Analysis 
%grid over possible values q
for j=1:length(q_series)
%define stage cost Q,R
Q=C'*C+q_series(j)*eye(n_x);
R=r_0*eye(n_u);
%1. use theory for positive definite (PDF) stage costs with \ell=sigma (Theorem 7)
%a) no terminal cost 
terminal=0;
[N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,0,M_extended);
N_Q_pdf_UCON(j)=N_gamma_k;
%b) terminal weighting (Prop. 2)
terminal=1; 
[N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,omega,M_extended);
N_Q_pdf_omega(j)=N_gamma_k;
%c) terminal cost using finite-tail (Prop. 3)
terminal=2; 
[N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,omega,M_extended);
N_Q_pdf_tail(j)=N_gamma_k;

%2. use theory for detectable stage costs with W=sigma (Theorem 8)
%a) no terminal cost 
terminal=0;
[N_gamma_k,LP_equiv,N_grimm]=Analysis_detect(A,B,Q,R,N_max,epsilon_o_list,LP_validation,local,terminal,omega,M_extended);
N_Q_detect_UCON(j)=N_gamma_k; 
N_Q_detect_UCON_grimm(j)=N_grimm;
%b) terminal weighting (Prop. 2)
terminal=1; 
[N_gamma_k,LP_equiv]=Analysis_detect(A,B,Q,R,N_max,epsilon_o_list,LP_validation,local,terminal,omega,M_extended);
N_Q_detect_omega(j)=N_gamma_k; 
%c) terminal cost using finite-tail (Prop. 3)
terminal=2; 
[N_gamma_k,LP_equiv]=Analysis_detect(A,B,Q,R,N_max,epsilon_o_list,LP_validation,local,terminal,omega,M_extended);
N_Q_detect_tail(j)=N_gamma_k; 
end 
%% Make plots
if plots
figure(1)
loglog(q_series,N_Q_pdf_UCON,'b-','linewidth',2)
hold on
plot(q_series,N_Q_detect_UCON,'r-','linewidth',2)
loglog(q_series,N_Q_pdf_omega,'b--','linewidth',2)
loglog(q_series,N_Q_detect_omega,'r--','linewidth',2)
loglog(q_series,N_Q_pdf_tail,'b:','linewidth',2)
loglog(q_series,N_Q_detect_tail,'r:','linewidth',2)
%additionally highlight design for closed-loop
loglog(1e-1,5,'b+','linewidth',2,'markersize',10)
xlabel('$q$','interpreter','latex')
ylabel('$\underline{N}$','interpreter','latex')
axis([min(q_series),max(q_series),0.9,10^7])
set(gca, 'fontname','Arial','fontsize',16)
set(gca, 'YTick',[1 1e2 1e4,1e6])
print(['Figure/q_M' num2str(M)],'-depsc')
end
%% Analysis R
%same procedure, but now change input weighting r
for j=1:length(r_series)
%define stage cost Q,R
Q=C'*C+q_0*eye(n_x);
R=r_series(j)*eye(n_u);
%1. use theory for positive definite (PDF) stage costs with \ell=sigma (Theorem 7)
%a) no terminal cost 
terminal=0;
[N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,0,M_extended);
N_R_pdf_UCON(j)=N_gamma_k;
%b) terminal weighting (Prop. 2)
terminal=1;
[N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,omega,M_extended);
N_R_pdf_omega(j)=N_gamma_k;
%c) terminal cost using finite-tail (Prop. 3)
terminal=2;
[N_gamma_k,LP_equiv]=Analysis_PDF(A,B,Q,R,N_max,LP_validation,local,terminal,omega,M_extended);
N_R_pdf_tail(j)=N_gamma_k;

%2. use theory for detectable stage costs with W=sigma (Theorem 8)
%a) no terminal cost 
terminal=0;
[N_gamma_k,LP_equiv,N_grimm]=Analysis_detect(A,B,Q,R,N_max,epsilon_o_list,LP_validation,local,terminal,omega,M_extended);
N_R_detect_UCON(j)=N_gamma_k; %save horizon
N_R_detect_UCON_grimm(j)=N_grimm;%for comparison, also save horizon computed using simpler bounds by Grimm et al.
%b) terminal weighting (Prop. 2)
terminal=1;
[N_gamma_k,LP_equiv]=Analysis_detect(A,B,Q,R,N_max,epsilon_o_list,LP_validation,local,terminal,omega,M_extended);
N_R_detect_omega(j)=N_gamma_k; 
%c) terminal cost using finite-tail (Prop. 3)
terminal=2;
[N_gamma_k,LP_equiv]=Analysis_detect(A,B,Q,R,N_max,epsilon_o_list,LP_validation,local,terminal,omega,M_extended);
N_R_detect_tail(j)=N_gamma_k; 
 
end
%% Make plots
if plots
figure(2)
loglog(r_series,N_R_pdf_UCON,'b-','linewidth',2)
hold on
loglog(r_series,N_R_detect_UCON,'r-','linewidth',2)
loglog(r_series,N_R_pdf_omega,'b--','linewidth',2)
loglog(r_series,N_R_detect_omega,'r--','linewidth',2)
loglog(r_series,N_R_pdf_tail,'b:','linewidth',2)
loglog(r_series,N_R_detect_tail,'r:','linewidth',2) 
loglog(1.7,5,'r+','linewidth',2,'markersize',10)
xlabel('$r$','interpreter','latex')
ylabel('$\underline{N}$','interpreter','latex')
axis([min(r_series),max(r_series),0.9,10^7])
set(gca, 'fontname','Arial','fontsize',16)
set(gca, 'YTick',[1 1e2 1e4,1e6])
print(['Figure/r_M' num2str(M)],'-depsc')
end
%%
toc(t)%record offline computation time