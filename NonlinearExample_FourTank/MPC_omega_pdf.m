function [solver,lb,ub,con_lb,con_ub]=MPC_omega_pdf(k,param)
%generata NLP for standard MPC problem with horizon N
%include penalty omega for last stage cost (grune10)
%% import
import casadi.*
% set parameters
%param=Tank_param();  
param.N=k;
%decision variables
y=MX.sym('y',param.N*param.m+(param.N+1)*param.n);
%% compute cost+constraints
obj = MX(0);
obj=costfunction(param,y);
%
[c, ceq] = nonlinearconstraints(param,y);
con=[c;ceq];
con_bound=zeros(param.N*param.n,1);
con_lb=[con_bound];
con_ub=[con_bound];
%
%% set box constraints 
lb=[repmat(param.x_min,param.N+1,1);repmat(param.u_min,param.N,1)];
ub=[repmat(param.x_max,param.N+1,1);repmat(param.u_max,param.N,1)];
 %% compute guess
%optimization
nlp = struct('x', y, 'f', obj, 'g', con);
opts=struct;
opts.print_time=0;
opts.ipopt.print_level=0;
solver = nlpsol('solver', 'ipopt', nlp,opts);
% to solve problem, call:
%lb(index_init)=x(:,k);
%ub(index_init)=x(:,k);
%y_init=zeros(size(y));
%solver('x0' , y_init,... % solution guess
%                 'lbx', lb,...           % lower bound on x
%                 'ubx', ub,...           % upper bound on x
%                 'lbg', con_lb,...           % lower bound on g
%                 'ubg', con_ub);             % upper bound on g
end
%%
function cost = costfunction(param,y)
    % Formulate the cost function to be minimized   
    cost = 0;   
x=y(1:param.n*(param.N+1));
u=y(param.n*(param.N+1)+1:param.n*(param.N+1)+param.N*param.m);
% Build the cost by summing up the stage cost and the terminal cost
    for k=1:param.N
        x_k=x(param.n*(k-1)+1:param.n*k);
        u_k=u(param.m*(k-1)+1:param.m*k);
        ell=runningcosts(x_k, u_k, param.x_0,param.u_0, param.Q, param.R);
        cost = cost + ell;
          
    end
    %terminal cost - no
    x_N=x(param.n*param.N+1:param.n*(param.N+1));
    ell=runningcosts(x_N, param.u_0, param.x_0,param.u_0, param.Q, param.R);
    cost = cost + param.omega*ell;
    
end


%%
function [c, ceq] = nonlinearconstraints(param,y) 
%    % Introduce the nonlinear constraints also for the terminal state  
x=y(1:param.n*(param.N+1));
u=y(param.n*(param.N+1)+1:param.n*(param.N+1)+param.N*param.m);
    c = [];
   ceq = [];
   % constraints along prediction horizon
    for k=1:param.N
        x_k=x((k-1)*param.n+1:k*param.n);
        x_new=x(k*param.n+1:(k+1)*param.n);        
        u_k=u((k-1)*param.m+1:k*param.m);
        %dynamic constraint
        ceqnew=x_new - dynamic(x_k, u_k,param);
        ceq = [ceq; ceqnew];
        %nonlinear constraints on state and input could be included here
        %c=[c cnew];
    end  
     %terminal set constraint - not in use
     %ceqnew=x_new-r(1:param.n);
     %ceq=[ceq;ceqnew];
end 