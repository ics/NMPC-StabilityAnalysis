
function param=Tank_param()
%load model constants of four tank model
param.n = 4; %number of states
param.m = 2; %number of inputs
%set parameters (units: cm, s, ml)
%numerical values from [“Nonlinear model predictive control of a four tank system: An experimental stability study, Raff et al. 2006]
param.g=9.81*1e2;%gravity
param.a_1=0.233;
param.a_2=0.242; 
param.a_3=0.127; 
param.a_4=0.127; 
param.A_1=50.27;
param.A_2=50.27; 
param.A_3=28.27; 
param.A_4=28.27; 
param.gamma_1=0.4;
param.gamma_2=0.4;

%compute directly relevant constants (cf. compact model equation in Section
%VII.B)
param.c_1=param.a_1/param.A_1*sqrt(2*param.g);
param.c_13=param.a_3/param.A_1*sqrt(2*param.g);
param.c_u1=param.gamma_1/param.A_1;
param.c_2=param.a_2/param.A_2*sqrt(2*param.g);
param.c_24=param.a_4/param.A_2*sqrt(2*param.g);
param.c_u2=param.gamma_2/param.A_2;
param.c_3=param.a_3/param.A_3*sqrt(2*param.g);
param.c_u3=(1-param.gamma_2)/param.A_3;
param.c_4=param.a_4/param.A_4*sqrt(2*param.g);
param.c_u4=(1-param.gamma_1)/param.A_4;
%set equlibrium level:
param.x_0=[14;14;14.2;21.3];
param.u_0=[43.4;35.4]; 
%set input constrains
param.u_min=[0;0];
param.u_max=param.u_0+[16.6;24.6];
%state constraints (not relevant for the following experiments)
param.x_min=param.x_0+[-6.5;-6.5;-10.7;-16.8];
param.x_max=param.x_0+[14;14;13.8;6.7];
%output map
param.C=[eye(2),zeros(2)];
%initial stage cost
param.Q_init=param.C'*param.C; 
param.R_init=1e-2*eye(param.m);
%sampling/discretization time
param.delta=3;%seconds
param.nu=2;%lag (for IOSS Lyap)
%% Make steady-state exact
%slightly modify x_0, s.t. exact steady-state
 param.x_0(3)=(param.c_u3*param.u_0(2)/param.c_3)^2;
 param.x_0(4)=(param.c_u4*param.u_0(1)/param.c_4)^2;
 param.x_0(1)=((param.c_u1*param.u_0(1)+param.c_13*sqrt(param.x_0(3)))/param.c_1)^2;
 param.x_0(2)=((param.c_u2*param.u_0(2)+param.c_24*sqrt(param.x_0(4)))/param.c_2)^2;
end
