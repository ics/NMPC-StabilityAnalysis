%simulate different MPC schemes and plot results
clear all 
close all
clc
T_max=100; %simulation time
simulated=false;%boolean: if simulations are done once, then we can directly load old results; only need to re-do if something changes
param=Tank_param();  
%% 1. 
%Simulate unstable initial MPC design
if simulated%can be used to load already simulated results instead
     load('results/unstable')   
else
    %load specified tuning (Q,R,N)
    param=Tank_param();  
    x_init=diag([1,1,1.5,1.5])*param.x_0;
    N_unstable=14;
    param.Q=param.Q_init;
    param.R=param.R_init;
    [solver_unstable,lb_unstable,ub_unstable,con_lb_unstable,con_ub_unstable]=MPC(N_unstable,param);%set up MPC solver
    x_unstable=x_init;
    %get initial condition
        for k=1:T_max
        [V_N,u_unstable(:,k)]=MPC_evaluate(x_unstable(:,k),N_unstable,solver_unstable,lb_unstable,ub_unstable,con_lb_unstable,con_ub_unstable,param);%solve NLP
        x_unstable(:,k+1)=dynamic(x_unstable(:,k),u_unstable(:,k),param);%simulate
        end
    Q_unstable=param.Q;
    R_unstable=param.R;
    save('results/unstable','x_unstable','u_unstable','Q_unstable','R_unstable','N_unstable')    %save results
end 
%% 2. 
%Simulate MPC designed based on detectability and with terminal cost
if simulated
    load('results/detect')
else    
    param=Tank_param();  
    x_init=diag([1,1,1.5,1.5])*param.x_0;
    %load specified tuning (Q,R,N)
    N_detect=23;
    param.Q=param.Q_init+0.05*eye(param.n);
    param.R=param.R_init;
    param.omega=1e3;
    param.nu=2;
    [solver_detect,lb_detect,ub_detect,con_lb_detect,con_ub_detect]=MPC_omega_detect(N_detect,param);
    x_detect=x_init;
    %get initial condition
        for k=1:T_max
        [V_N,u_detect(:,k)]=MPC_evaluate(x_detect(:,k),N_detect,solver_detect,lb_detect,ub_detect,con_lb_detect,con_ub_detect,param);
        x_detect(:,k+1)=dynamic(x_detect(:,k),u_detect(:,k),param);
        end
    Q_detect=param.Q;
    R_detect=param.R;
    save('results/detect','x_detect','u_detect','Q_detect','R_detect','N_detect')    
end
%% 3.
%Simulate MPC designed based on positive definite cost and with terminal cost
if simulated
    load('results/pdf')
else
param=Tank_param();  
x_init=diag([1,1,1.5,1.5])*param.x_0;
N_pdf=10;
param.Q=param.Q_init+0.1*eye(param.n);
param.R=param.R_init;
param.omega=1e3;
[solver_pdf,lb_pdf,ub_pdf,con_lb_pdf,con_ub_pdf]=MPC_omega_pdf(N_pdf,param);
x_pdf=x_init;
%get initial condition
    for k=1:T_max
    [V_N,u_pdf(:,k)]=MPC_evaluate(x_pdf(:,k),N_pdf,solver_pdf,lb_pdf,ub_pdf,con_lb_pdf,con_ub_pdf,param);
    x_pdf(:,k+1)=dynamic(x_pdf(:,k),u_pdf(:,k),param);
    end
Q_pdf=param.Q;
R_pdf=param.R;
save('results/pdf','x_pdf','u_pdf','Q_pdf','R_pdf','N_pdf')    
end
%% plot
close all
figure(1)
hold on
plot((x_unstable(1:2,:)-0*param.x_0(1:2))','black','linewidth',2) 
plot((x_pdf(1:2,:)-0*param.x_0(1:2))','b-','linewidth',2)
plot((x_detect(1:2,:)-0*param.x_0(1:2))','r-','linewidth',2)
plot([0,T_max],param.x_0(1)*[1,1],'black--','linewidth',2)
plot([0,T_max],param.x_0(2)*[1,1],'black--','linewidth',2)
xlabel('$k$','interpreter','latex')
ylabel('$x_1$, $x_3$ [cm]','interpreter','latex')
axis([0,T_max*0.7,13.2,15.15])
set(gca, 'fontname','Arial','fontsize',16)
print('Figure/closedloop_tank','-depsc')
%% 