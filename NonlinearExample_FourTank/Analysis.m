function [N_pdf_gamma_k,N_detect_gamma_k]= Analysis(param,omegas)
%%Ouput
%N_pdf_gamma_k: sufficient horizon with LP analysis based on positive definite stage cost (Thm. 7)
%N_detect_gamma_k: sufficient horizon with LP analysis based on detectable stage cost (Thm. 8)
%constants are approximated using gridding
%Also, different horizon bounds for different terminal weights omega

%initialize
gamma_detect=zeros(param.N_max+1,length(omegas));
gamma_pdf=zeros(param.N_max+1,length(omegas));
ell=zeros(param.N_max+1,1);
%grid state space 
for x1=linspace(param.x_min_grid(1),param.x_max_grid(1),param.n_x_grid)
for x2=linspace(param.x_min_grid(2),param.x_max_grid(2),param.n_x_grid)
for x3=linspace(param.x_min_grid(3),param.x_max_grid(3),param.n_x_grid)
for x4=linspace(param.x_min_grid(4),param.x_max_grid(4),param.n_x_grid)
    x=[x1;x2;x3;x4];
    cost=0; 
    %for each state x, simulate open-loop over k-steps and add terminal
    %cost to compute a bound gamma
    W = storage_eval(x,param);%evaluate storage function (cf. footnote 8)
         for k=1:param.N_max
                ell(k)=runningcosts(x, param.u_0, param.x_0, param.u_0, param.Q, param.R);
                if k>1&&param.nu==2
                W_temp=ell(k)+ell(k-1)/param.nu; %evaluate storage function at state k+1 using last nu=2 stage costs
                else
                W_temp=storage_eval(x,param); %not programmed for other values-> optimize (takes longer)
                end
                for j=1:length(omegas)
                %compute cost controllability constant (Ass.3) for
                %a) terminal weight omega*ell; and sigma=\ell
                gamma_pdf(k,j)=max((cost+omegas(j)*ell(k))/ell(1),gamma_pdf(k,j));
                %b) terminal weight omega=W, and sigma=W (index shifted by
                %one here, for ease of implementation with formula for
                %W_temp above)
                gamma_detect(k+1,j)=max((cost+ell(k)+omegas(j)*W_temp)/W,gamma_detect(k+1,j));
                %simulate over k-steps, sum cost
                end
                cost=cost+ell(k);
                x=dynamic(x,param.u_0,param);
         end
end
end
end
end
%%  Analysis - pdf (Theorem 7)
if min(eig(param.Q))==0 
    N_pdf_gamma_bar=inf*omegas; 
    N_pdf_gamma_k=inf*omegas;
else
    %initialize
    gamma_pdf_bar=inf*omegas;
    N_pdf_gamma_bar=inf*omegas;
    N_pdf_gamma_k=inf*omegas;
    alpha_N_pdf_gamma_k=inf(param.N_max-1,length(omegas));
    epsilon_pdf_f=inf*omegas;
    %loop over different terminal weighting
    for j=1:length(omegas)
        epsilon_pdf_f(j)=max(gamma_pdf(2,j)./omegas(j)-1,0);%careful, gamma(k+1) corresponds to gamma_k
        gamma_pdf_bar(j)=max(gamma_pdf(:,j));%maximal value for simple bound  
        %simple formula using gamma_bar (Inequality (18)); this bound is used, if N>N_max
        N_pdf_gamma_bar(j)=ceil(1+(log(gamma_pdf_bar(j))-log(1+1/epsilon_pdf_f(j)))/(log(gamma_pdf_bar(j))-log(gamma_pdf_bar(j)-1)));
        %compute alpha_N according to Equation (16) from Thm. 7
        for N=1:param.N_max-1
           alpha_N_pdf_gamma_k(N,j)=1-(gamma_pdf(N+1,j)-1)*prod((gamma_pdf(2+1:N+1,j)-1)./gamma_pdf(2+1:N+1,j))/...
               (1+1/epsilon_pdf_f(j)- prod((gamma_pdf(2+1:N+1,j)-1)./gamma_pdf(2+1:N+1,j)));
        end
        if omegas(j)==0
           alpha_N_pdf_gamma_k(1,j)=-inf; 
        end
        %make sure that monotonically by checking oposite
        if alpha_N_pdf_gamma_k(end,j)<0 || isnan(alpha_N_pdf_gamma_k(end,j))
          %in case alpha<0 for all N, then we did not find a stabilizing horizon
           N_pdf_gamma_k(j)=N_pdf_gamma_bar(j); 
        elseif alpha_N_pdf_gamma_k(1,j)>0
          %in case alpha(1)>0 then N=1 is already sufficient
           N_pdf_gamma_k(j)=1;     
        else
            N_pdf_gamma_k(j)=find(alpha_N_pdf_gamma_k(:,j)<=0,1,'last')+1;
        end
    end
end
%%  Analysis - detectable cost (Theorem 8)
%initialize
gamma_detect_bar=inf*omegas;
N_grimm=inf*omegas;
N_detect_gamma_bar=inf*omegas;
N_detect_gamma_k=inf*omegas;
alpha_N_detect_gamma_k=inf(param.N_max-1,length(omegas));
epsilon_f_detect=inf*omegas;
%loop over different terminal weighting
for j=1:length(omegas)
    epsilon_f_detect(j)=max(gamma_detect(2,j)./omegas(j)-1,0);%careful, gamma(k+1) corresponds to gamma_k
    gamma_detect_bar(j)=max(gamma_detect(:,j));  
    %for comparison, also compute using simpler formula, similar to [Grimm et al.
    %2005], cf. Theorem 5
    N_grimm(j)=ceil(1+1/(1+1/epsilon_f_detect(j))*gamma_detect_bar(j)*(gamma_detect_bar(j)+1)/param.epsilon_o^2);
    %simple analytic formula using gamma_bar (this bound is used, if N>N_max)
    N_detect_gamma_bar(j)=ceil(1+(log(gamma_detect_bar(j))-log(param.epsilon_o)-log(1+1/epsilon_f_detect(j)))/(log(1+gamma_detect_bar(j))-log(gamma_detect_bar(j)+param.eta)));
     %compute alpha_N according to Equation (19) from Thm. 8
    for N=1:param.N_max-1
        temp=prod( (param.eta+gamma_detect(2+1:N+1,j))./(1+gamma_detect(2+1:N+1,j)));
       alpha_N_detect_gamma_k(N,j)=1-gamma_detect(1+1,j)/param.epsilon_o*(gamma_detect(N+1,j)+param.eta)*temp/...
           (1+1/epsilon_f_detect(j)+gamma_detect(1+1,j)*(1+1/epsilon_f_detect(j)- temp)); 
    end
    %make sure that monotonically by checking oposite
    N_detect_gamma_k(j)=find(alpha_N_detect_gamma_k(:,j)<=0,1,'last')+1;
    if alpha_N_detect_gamma_k(end,j)<0
      %in case alpha<0 for all N, then we did not find a stabilizing horizon;
      %choose conservative with gamma_bar
       N_detect_gamma_k(j)=N_detect_gamma_bar(j); 
    elseif alpha_N_detect_gamma_k(1,j)>0
      %in case alpha(1)>0 then N=1 is already sufficient
       N_detect_gamma_k(j)=1;     
    end 
end
end