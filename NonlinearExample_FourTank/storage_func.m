
function solver_storage = storage_func(param)
%compute W(x) for given x and lag nu=2  (hand coded for lag nu=2)
%cf. footnote 7 for formula
import casadi.*
u_0=MX.sym('u_0',param.m);
u_1=MX.sym('u_1',param.m);
x_0=MX.sym('tilde_x',param.n);
x_1=dynamic(x_0,u_0,param);
x_2=dynamic(x_1,u_1,param);
con=[x_2]; %x_2=x is later imposed
cost = 1/2*runningcosts(x_0,u_0,param.x_0,param.u_0,param.Q,param.R)...
       +1*runningcosts(x_1,u_1,param.x_0,param.u_0,param.Q,param.R);
       y=[x_0;u_0;u_1];
nlp_storage = struct('x', y, 'f', cost, 'g', con);
opts=struct;
opts.print_time=0;
opts.ipopt.print_level=0;

solver_storage = nlpsol('solver', 'ipopt', nlp_storage,opts); 

end