%this script computes a sufficiently long horizon \underline{N} for
%different stage costs \ell, using the results in Theorem 7 and Theorem 8
%in [Stability and performance analysis of NMPC: Detectable stage costs and general terminal costs]
clear all
close all
clc
%load system parameters
param=Tank_param();
 %% set consants
param.epsilon_o=1/param.nu;
param.eta=1-param.epsilon_o;
param.gamma_o=1;
param.n_x_grid=15%how many gridpoints in each state dimension
param.x_max_grid=param.x_0+5;%specify range of grid 
param.x_min_grid=param.x_0-5;
q_grid=sort([0,10.^linspace(-4,0,6-1),2e-2,5e-2]);%grid over stage cost values
param.n_q_grid=length(q_grid);
param.N_max=100; 
t=tic;%record time
for j=1:length(q_grid)%loop over stage cost
    param.Q=param.Q_init+q_grid(j)*eye(param.n);%set Q,R matrices
    param.R=param.R_init;
    %define a solver to compute storage function, cf. footnote 8
    param.solver_storage = storage_func(param);            
    omegas=[0,1,10,100,1e3,1e4,1e5];%specify range of terminal weightings
    [N_pdf_gamma_k(j,:),N_detect_gamma_k(j,:)]= Analysis(param,omegas);%determine horizon using both methods
    [toc(t),j]
end
%% Plots
%N_pdf(q)
figure(10)
q_grid_plot=q_grid;
q_grid_plot(1)=q_grid(2)*1e-1;%to get finite value at start
N_pdf_gamma_k(isinf(N_pdf_gamma_k))=10^30;%finite/large value to visualize inf/nan
N_pdf_gamma_k(isnan(N_pdf_gamma_k))=10^30;%finite/large value to visualize inf/nan
%sigma=\ell, 
loglog(q_grid_plot,N_pdf_gamma_k(:,1),'b-','linewidth',2)
hold on
%sigma=\ell,  with terminal  
loglog(q_grid_plot,N_pdf_gamma_k(:,5),'b--','linewidth',2)
%sigma=W: red
loglog(q_grid_plot,N_detect_gamma_k(:,1),'r-','linewidth',2)
%%sigma=W with terminal  
loglog(q_grid_plot,N_detect_gamma_k(:,5),'r--','linewidth',2)
%unstable
plot(q_grid_plot(6),N_detect_gamma_k(6,5),'red+','linewidth',2,'markersize',20)
plot(q_grid_plot(7),10,'blue+','linewidth',2,'markersize',20)

xlabel('$q$','interpreter','latex')
ylabel('$\underline{N}$','interpreter','latex')
axis([q_grid(2)*0.1,max(q_grid),0.9,10^5])
set(gca, 'fontname','Arial','fontsize',16)
set(gca, 'YTick',[1 1e1 1e2 1e3 1e4])
set(gca, 'XTick',[1e-5,1e-4,1e-3,1e-2,1e-1,1], 'xticklabel',{0,'10^{-4}','10^{-3}','10^{-2}','10^{-1}',1})
print('Figure/q_tank','-depsc')
%%
%N_detect(q)
figure(1)
loglog(q_grid,N_detect_gamma_k(:,1),'b-o','linewidth',2)
hold on
plot(q_grid,N_detect_gamma_k(:,3),'m--o','linewidth',2)
plot(1e-3,14,'black+','linewidth',2,'markersize',20)
plot(q_grid(4),N_detect_gamma_k(4,3),'m+','linewidth',2,'markersize',20)
plot(q_grid(4),N_detect_gamma_k(4,2),'b+','linewidth',2,'markersize',20)
xlabel('$q$','interpreter','latex')
ylabel('$\underline{N}$','interpreter','latex')
axis([min(q_grid),max(q_grid),0.9,10^4])
set(gca, 'fontname','Arial','fontsize',16)
set(gca, 'YTick',[1 1e1 1e2 1e3])
print('Figure/q_tank','-depsc')
%% analysis - pdf
figure(2)
loglog(q_grid,N_pdf_gamma_k(:,1),'b-o','linewidth',2)
hold on
plot(q_grid,N_pdf_gamma_k(:,3),'m--o','linewidth',2)
plot(1e-3,14,'black+','linewidth',2,'markersize',20)
xlabel('$q$','interpreter','latex')
ylabel('$\underline{N}$','interpreter','latex')
axis([min(q_grid),max(q_grid),0.9,10^6])
set(gca, 'fontname','Arial','fontsize',16)
set(gca, 'YTick',[1 1e1 1e2 1e3,1e4,1e5])
print('Figure/q_tank_pdf','-depsc')