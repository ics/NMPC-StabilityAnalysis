
function W = storage_eval(x,param)
%evaluate W(x) using implicit definition in footnote 7

%warmstart
y_guess=[param.x_0;param.u_0;param.u_0];
con_lb=x;
con_ub=x;
%optimize
res = param.solver_storage('x0' , y_guess,... 
                 'lbg', con_lb,...           
                 'ubg', con_ub);       
W=full(res.f);
end