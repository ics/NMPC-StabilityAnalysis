function [V_N,u_0]=MPC_evaluate(x,N,solver,lb,ub,con_lb,con_ub,param)
%solve MPC with NLP 
lb(1:param.n)=x;
ub(1:param.n)=x;
y_init=ones(size(lb));%0 not good!
res=solver('x0' , y_init,... % solution guess
                 'lbx', lb,...           % lower bound on x
                 'ubx', ub,...           % upper bound on x
                 'lbg', con_lb,...           % lower bound on g
                 'ubg', con_ub);             % upper bound on g
V_N=full(res.f);
y_opt=full(res.x);             
u_0=y_opt(param.n*(N+1)+1:param.n*(N+1)+param.m);
end