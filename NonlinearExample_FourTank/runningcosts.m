
function cost = runningcosts(x, u, x_eq, u_eq, Q, R)
    % Provide the running cost    
    cost = (x-x_eq)'*Q*(x-x_eq) + (u-u_eq)'*R*(u-u_eq);
    
end